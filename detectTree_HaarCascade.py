import cv2
import argparse
import os
import numpy as np
from imutils.object_detection import non_max_suppression
from datetime import datetime
import sys

parser = argparse.ArgumentParser()
parser.add_argument("--input_path", help="path of input image or video files", default="")
parser.add_argument("--mode", help="mode with the input data type, it can be ['image', 'folder', 'video']", default="")
parser.add_argument("--save", help="flag whether the result will be saved or not", default=True, type=bool)
arg = parser.parse_args()


HIST_STEPS = 32
g_my_cascade = None
g_upsampling = None
g_distance = None


def _getColorFeature(sub):

    hist_blue = cv2.calcHist([sub], [0], None, [HIST_STEPS], [0, 256])
    hist_green = cv2.calcHist([sub], [1], None, [HIST_STEPS], [0, 256])
    hist_red = cv2.calcHist([sub], [2], None, [HIST_STEPS], [0, 256])

    blue = np.argmax(hist_blue) * 255 / HIST_STEPS + 0.5 * 255 / HIST_STEPS
    green = np.argmax(hist_green) * 255 / HIST_STEPS + 0.5 * 255 / HIST_STEPS
    red = np.argmax(hist_red) * 255 / HIST_STEPS + 0.5 * 255 / HIST_STEPS

    return [blue, green, red]


negative_color = [110, 119, 92]
positive_color = [76, 80, 85]


def _getLabelFromColor(color):

    # dis_pos, dis_neg = 0.0, 0.0
    # for i in range(len(color)):
    #     dis_neg += (color[i] - negative_color[i]) ** 2
    #     dis_pos += (color[i] - positive_color[i]) ** 2
    # if dis_pos > dis_neg:
    #     return 'neg'
    # else:
    #     return 'pos'

    if color.index(max(color)) == 1:
        return 'neg'
    # elif color[0] > color[2]:
    #     return 'neg'
    else:
        return 'pos'


def detector(frame):

    global g_my_cascade, g_distance, g_upsampling

    height, width = frame.shape[:2]

    # detect positive rect
    pos_rects = []
    for detector in g_my_cascade:
        rects = detector.detectMultiScale(frame, g_upsampling, g_distance)

        for (x, y, w, h) in rects:
            sub_img = frame[int(y + h / 3):int(y + h * 2 / 3), int(x + w / 3):int(x + w * 2 / 3)]
            # sub_img = frame[int(y):int(y + h), int(x):int(x + w)]
            color = _getColorFeature(sub_img)
            if _getLabelFromColor(color) == 'pos' and y > height / 4:
                pos_rects.append([x, y, x + w, y + h])

    # merge the detected rects
    boxs = np.array(pos_rects)
    pick = non_max_suppression(boxs, probs=None, overlapThresh=0.05)

    # drawing the rect for boundary of detected tree
    for (x1, y1, x2, y2) in pick:
        cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 255), 2)

    return frame


def videoDetectTree(video_path, save, skip=3):

    if video_path == '0':
        cap = cv2.VideoCapture(int(video_path))
    else:
        cap = cv2.VideoCapture(video_path)

    if save:
        out_path = 'Result.avi'
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        # fourcc = cv2.VideoWriter_fourcc(*'x264')
        out = cv2.VideoWriter(out_path, fourcc, 25.0,
                              (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    if not cap.isOpened():
        raise Exception("Can not open this video file.", video_path)

    cur_frame = 0
    while True:

        _, frame = cap.read()
        cur_frame += 1
        if cur_frame % skip == 0:  # take every 3 frames
            start = datetime.now()

            out_frame = detector(frame)
            cv2.imshow("result", out_frame)

            end = datetime.now()
            str = "\r fps %0.1f" % (skip * 10 ** 6 / (end - start).microseconds)
            sys.stdout.write(str)
            sys.stdout.flush()

            if save:
                out.write(out_frame)

        if cv2.waitKey(1) == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
    if save:
        out.release()


def imageDetectTree(im_path, save):

    # Load the image
    img = cv2.imread(im_path)

    out_img = detector(img)

    cv2.imshow("Result", out_img)
    cv2.waitKey(0)

    if save:
        in_fn, ext = os.path.splitext(im_path)
        res_path = in_fn + '_result' + ext
        cv2.imwrite(res_path, out_img)


if __name__ == '__main__':

    ###
    arg.mode = 'video'
    arg.input_path = './orchard/video/WIN_20170710_07_11_41_Pro.MP4'
    arg.save = True
    ###

    # global g_my_cascade, g_upsampling, g_distance

    print(cv2.__version__)
    g_my_cascade = []
    # mode_1st_layer = './model/TreeHaar_1st.xml'
    # if not os.path.isfile(mode_1st_layer):
    #     raise Exception("There is no such xml files", mode_1st_layer)
    # g_my_cascade.append(cv2.CascadeClassifier(mode_1st_layer))
    #
    # mode_2nd_layer = './model/TreeHaar_2nd.xml'
    # if not os.path.isfile(mode_2nd_layer):
    #     raise Exception("There is no such xml files", mode_2nd_layer)
    # g_my_cascade.append(cv2.CascadeClassifier(mode_2nd_layer))

    mode_1st_layer = './model/myhaar_20_40_20.xml'
    if not os.path.isfile(mode_1st_layer):
        raise Exception("There is no such xml files", mode_1st_layer)
    g_my_cascade.append(cv2.CascadeClassifier(mode_1st_layer))

    """ ++++++++++++++++++++++++++++++++++++++++++++ """
    """ params """
    g_upsampling = 1.05  # 1.005
    g_distance = 1
    bSave = arg.save
    skip = 3
    """ ++++++++++++++++++++++++++++++++++++++++++++ """

    if arg.mode == "" or arg.input_path == "":
        raise Exception("The correct path and mode should be.", arg.mode, arg.input_path)

    elif arg.mode == "image":
        img_path = arg.input_path
        if not os.path.isfile(img_path, bSave):
            raise Exception("There is no such image file", img_path)
        imageDetectTree(im_path)

    elif arg.mode == "folder":
        folder = arg.input_path
        if not os.path.isdir(folder):
            raise Exception("There is no such image folder", folder)
        for f in os.listdir(folder):
            imageDetectTree(os.path.join(folder, f), bSave)

    elif arg.mode == "video":
        video = arg.input_path
        if not os.path.isfile(video):
            raise Exception("There is no such video file", video)
        videoDetectTree(video, bSave, skip)

    elif arg.mode == "camera":
        video = '0'
        videoDetectTree(video, bSave, skip)
